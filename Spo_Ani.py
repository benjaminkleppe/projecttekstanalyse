import os
import sys
import nltk
from nltk.tag.stanford import StanfordNERTagger
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import wordnet
from nltk.wsd import lesk
from nltk import word_tokenize
import glob
import sys 
import spacy
nlp = spacy.load("en")


"""def getEntity(file):
    linelist = []
    total_list = []
    spacylist = []
    ent_list = []
    spacylabels = ["GPE", "PERSON", "ORG", "LOC", "WORK_OF_ART"]
    with open(file) as f:
        data = f.readlines()
    for line in data:
        newline = line.split()
        total_list.append(newline)
        linelist.append(newline[3])
        long_line = ' '.join(linelist)"""

def Spo_Ani(file):
	nouns = ["NN", "NNS", "NNP"]
	word_list = []
	with open(file) as f:
		data = f.readlines()
		noun = []
	for line in data:
		newline = line.split()
		word_list.append((newline[3], newline[4]))
		for x1, x2 in word_list:
			lem = WordNetLemmatizer()
			synset = wordnet.synsets(lem.lemmatize(x1, wordnet.NOUN), pos="n")
			if x2 in nouns:
				hyper = lambda s: s.hypernyms()
				ss = wordnet.synsets(x1)[0]
				hyper_list = list(ss.closure(hyper))
				for x in hyper_list:
					if "Synset('animal.n.01')" == str(x):
						x2 = "ANI"
						return x1, x2
					if "Synset('sport.n.01')" == str(x):
						x2 = "SPO"
						return x1, x2
			else:
				pass

                           
def main():
    file = 'en.tok.off.pos'
    print(Spo_Ani(file))


if __name__ == "__main__":
    main()

