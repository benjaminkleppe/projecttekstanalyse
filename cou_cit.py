import os
import sys
import nltk
from nltk.tag.stanford import StanfordNERTagger
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import wordnet
from nltk.wsd import lesk
from nltk import word_tokenize
import spacy
nlp = spacy.load("en")
import wikipedia #first do: pip install wikipedia
import pycountry #first do: pip install pycountry
from nltk.wsd import lesk
from nltk import word_tokenize

'''If you encounter an error like:

"UserWarning: No parser was explicitly specified, so I'm using the best available HTML parser for this system ("lxml"). 
This usually isn't a problem, but if you run this code on another system, or in a different virtual environment, 
it may use a different parser and behave differently."

("lxml" could also be "html5lib")

Then change the wikipedia.py file: 
- find the line: lis = BeautifulSoup(html).find_all('li')
- change this to: lis = BeautifulSoup(html, 'html.parser').find_all('li')
'''


def cou_cit(file):
    with open(file) as f:
        data = f.readlines()
    tagtype = ""
    ner = 'Washington'
    try: #this works if the ner is exact the same as the list of countries in pycountry
        if pycountry.countries.get(name=ner):
            tagtype = "COU"
            try:
                url = wikipedia.page(str(ner)).url
            except wikipedia.exceptions.DisambiguationError as e: #if word has more than one wikipedia page
                url = wikipedia.page(str(e.options[0])).url
            print(url, tagtype)
        
    except KeyError: #for example: United Kingdom instad of England, if word is not in pycountry
        try:
            nerContent = wikipedia.page(str(ner)).content #print content of the wikipedia page of ner
            nerContentLower = nerContent.lower()
            nerContentFirstLine = nerContentLower.split(".")[0]
            cityTypes = [" city ","capital","town","vilage","settlement","parish","neighbourhood","neighborhood","borough","port","township","metropolis","quarter"]
            for cityType in cityTypes:
                if cityType in nerContentFirstLine:
                    tagtype = "CIT"
                    url = wikipedia.page(str(ner)).url
                    print(url, tagtype)
                else:
                    subdivisionTypes = ["state ","city-state","province","district","region","principality","municipality","sovereign","country"]
                    if tagtype == "":
                        for subdivisionType in subdivisionTypes:
                            if subdivisionType in nerContentFirstLine:
                                tagtype = "COU"
                                url = wikipedia.page(str(ner)).url
                                print(url, tagtype)
        except wikipedia.exceptions.DisambiguationError as e: #if word has more than one wikipedia page, met lesk of synsets
            list_text = []
            for lines in data:
                lines = lines.split()
                list_text.append(lines[3])
                joined_list = " ".join(list_text)
            sent = word_tokenize("I live in Washington, it is the capital of the United States in the District of Columbia and the tourist mecca.")
            word = ner
            pos = "n"
            good_synset = lesk(sent, word, pos) #gives back the good synset of noun
            
            list_pos_ss = []
            for ss in wordnet.synsets(ner, "n"):
                possible_ss = [ss, ss.definition()] #gives back all possible synsets with definition
                synset01 = [ss] #gives back all possible synsets
                
                for s in synset01:
                    list_pos_ss.append(ss) #makes list of all possible synsets (without defintion)
                
            for item in list_pos_ss:
                if item == good_synset: #checks whether good synset is in list of all possible synsets
                    good_def_list = item.definition().split() #gives back list of the definition of the good synset
                    
            cou_list = ["state","city-state","province","district","region","principality","municipality","sovereign","country"]
            cou_checker = (any(elem in cou_list for elem in good_def_list))
            if cou_checker:
                print("COU")
            else:
                print("CIT")
    
                    
     
            
            
def main():
    file = 'en.tok.off.pos'
    print(cou_cit(file))            
            
            
            
            
            
            
            
            
                                                                                
                #except wikipedia.exceptions.PageError:
                    #tagtype = "CIT"
                    #print(tagtype)
                    
                #nerContentFirstLine = nerContent.split(".")[0]
                #if "city" in nerContentFirstLine:
                    #tagtype = "CIT"
                    #url = wikipedia.page(str(ner)).url
                    #print(url, tagtype)
                #elif "state" in nerContentFirstLine or "province" in nerContentFirstLine or "district" in nerContentFirstLine or "region" in nerContentFirstLine:
                    #tagtype = "COU"
                    #url = wikipedia.page(str(ner)).url
                    #print(url, tagtype)
                #if tagtype == "":
                    #tagtype = "COU"
                    #try:
                        #url = wikipedia.page(str(ner)).url
                        #print(url, tagtype)
                    #except wikipedia.exceptions.DisambiguationError as e:
                        #for option in e.options:
                            #subdivisionTypes = ["state ","city-state","province","district","region","principality","municipality","sovereign"]
                            #for subdivisionType in subdivisionTypes:
                                #if subdivisionType in nerContentFirstLine:
                                    #url = wikipedia.page(str(option)).url
                                    #print(url, tagtype)
                    #print(tagtype)
        
		
		
if __name__ == "__main__":
    main()