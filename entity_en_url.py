# functie voor het inladen van files


import spacy
import wikipedia
nlp = spacy.load("en")


def getEntity(file):
    linelist = []
    total_list = []
    spacylist = []
    ent_list = []
    spacylabels = ["GPE", "PERSON", "ORG", "LOC", "WORK_OF_ART"]
    with open(file) as f:
        data = f.readlines()
    for line in data:
        newline = line.split()
        total_list.append(newline)
        linelist.append(newline[3])
        long_line = ' '.join(linelist)
        # een lange string met alle woorden

        clean_string = nlp(str(long_line))
    for ent in clean_string.ents:
        ent_list.append(ent)
    for ent in ent_list:
        label = ent.label_
        if label in spacylabels:
            if label == "LOC":
                site = getWikipage(ent)
                label = "NAT"
                spacylist.append([newline, label, site])
            elif label == "ORG":
                site = getWikipage(ent)
                label = "ORG"
                spacylist.append([newline, label, site])
            elif label == "PERSON":
                site = getWikipage(ent)
                label = "PER"
                spacylist.append([newline, label, site])
            elif label == "GPE":
                site = getWikipage(ent)
                label = "GPE"
                spacylist.append([newline, label, site])
            else:
                if label == "WORK_OF_ART":
                    site = getWikipage(ent)
                    label = "ENT"
                    spacylist.append([newline, label, site])
    return spacylist


def getWikipage(noun):
    try:
        websiteURL = wikipedia.page(str(noun)).url
        return websiteURL
    except wikipedia.exceptions.PageError:
        return ''
    except wikipedia.exceptions.DisambiguationError as e:
        try:
            websiteURL = wikipedia.page(str(e.options[0])).url
            return websiteURL
        except wikipedia.exceptions.DisambiguationError as b:
            websiteURL = wikipedia.page(str(b.options[0])).url
            return websiteURL


def main():
    file = 'en.tok.off.pos'
    print(getEntity(file))


if __name__ == "__main__":
    main()
