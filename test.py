# functie voor het inladen van files

import os
import sys
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import wordnet
import spacy
import wikipedia
nlp = spacy.load("en")
import sys
import nltk
from nltk.tag.stanford import StanfordNERTagger
from nltk.wsd import lesk
from nltk import word_tokenize
import wikipedia #first pip install wikipedia
import pycountry #first pip install pycountry



def getEntity(root, file):
    line_list = []
    total_list = []
    tag_list = []
    ent_list = []
    spacylabels = ["GPE", "PERSON", "ORG", "LOC", "WORK_OF_ART"]
    with open(file) as f:
        data = f.readlines()
    for line in data:
        newline = line.split()
        total_list.append(newline[0:5])
        tag_list.append(newline[4])
        long_line = ' '.join(line_list)
        # een lange string met alle woorden

        clean_string = nlp(str(long_line))
        index = -1
        flow = 0
        for ent in clean_string.ents:
            ent_list.append(ent)
        for word in line_list:
            index += 1
            if len(ent_list) > 0:
                noun = ent_list[0]
                label = noun.label_               
                if label not in spacylabels:
                    appendStrToEntFile(total_list[index], root)
                    del ent_list[0]
                    continue
                if word == str(noun).split()[0]:
                    sliced_noun = str(noun).split()
                    word_amount = len(sliced_noun)
                    index2=index
                    iterator=0
                    while iterator < word_amount:
                        if line_list[index2] == sliced_noun[iterator]:
                            iterator += 1
                            index2 += 1
                        else:
                            break
                    if iterator == word_amount:
                        flow = 1
                    else:
                        appendStrToEntFile(total_list[index], root)
                        continue
                if flow != 0:
                    if word_amount == 0:
                        label = Spo_Ani(word, tag_list[index])
                        if label != "":
                            total_list[index].append(label)
                            site = getWikipage(word)
                            if site != "":
                                total_list[index].append(site)
                            appendStrToEntFile(total_list[index], root)
                        else:
                            appendStrToEntFile(total_list[index], root)
                        del ent_list[0]
                        flow = 0
                    else:
                        if label == "LOC":
                            site = getWikipage(noun)
                            label = "NAT"
                        elif label == "ORG":
                            site = getWikipage(noun)
                            label = "ORG"
                        elif label == "PERSON":
                            site = getWikipage(noun)
                            label = "PER"
                        elif label == "GPE":
                            label = "GPE"
                            cou_city = cou_cit(file, noun)
                            if len(cou_city) == 2:
                                label = cou_city[0]
                                site = cou_city[1]
                            else:
                                label = cou_city[0]
                        else:
                            label == "WORK_OF_ART"
                            site = getWikipage(noun)
                            label = "ENT"
                        word_amount -= 1
                        total_list[index].append(label)
                        if site != "":
                            total_list[index].append(site)
                        appendStrToEntFile(total_list[index], root)
                else:
                    label = Spo_Ani(word, tag_list[index])
                    if label != "":
                        total_list[index].append(label)
                        site = getWikipage(word)
                        if site != "":
                            total_list[index].append(site)
                        appendStrToEntFile(total_list[index], root)
                    else:
                        appendStrToEntFile(total_list[index], root)
            else:
                label = Spo_Ani(word, tag_list[index])
                if label != "":
                    total_list[index].append(label)
                    site = getWikipage(word)
                    if site != "":
                        total_list[index].append(site)
                    appendStrToEntFile(total_list[index], root)
                else:
                    appendStrToEntFile(total_list[index], root)
        line_list = []
        total_list = []
        tag_list = []
        ent_list = []
    else:
        line_list.append(newline[3])


def getWikipage(noun):
    try:
        websiteURL = wikipedia.page(str(noun)).url
        return websiteURL
    except wikipedia.exceptions.PageError:
        return ''
    except wikipedia.exceptions.DisambiguationError as e:
        try:
            websiteURL = wikipedia.page(str(e.options[0])).url
            return websiteURL
        except wikipedia.exceptions.DisambiguationError as b:
            websiteURL = wikipedia.page(str(b.options[0])).url
            return websiteURL


def Spo_Ani(noun, pos_tag):
	nouns = ["NN", "NNS", "NNP"]
	word_list = []
	new_noun = noun.split()
	noun_name = noun
	noun_tag = pos_tag
	lem = WordNetLemmatizer()
	synset = wordnet.synsets(lem.lemmatize(noun_name, wordnet.NOUN), pos="n")
	if noun_tag in nouns:
                hyper = lambda s: s.hypernyms()
                ss = wordnet.synsets(noun_name)[0]
                hyper_list = list(ss.closure(hyper))
                for x in hyper_list:
                        if "Synset('animal.n.01')" == str(x):
                                label = "ANI"
                                return label
                        if "Synset('sport.n.01')" == str(x):
                                label = "SPO"
                                return label


def cou_cit(file, noun):
    with open(file) as f:
        data = f.readlines()
    tagtype = ""
    cc_noun = noun
    try: #this works if the cc_noun is exact the same as the list of countries in pycountry
        if pycountry.countries.get(name=cc_noun):
            tagtype = "COU"
            site = getWikipage(cc_noun)
            return tagtype, site
        
    except KeyError: #for example: United Kingdom instead of England, if word is not in pycountry
        try:
            cc_nounContent = wikipedia.page(str(cc_noun)).content #print content of the wikipedia page of cc_noun
            cc_nounContentLower = cc_nounContent.lower()
            cc_nounContentFirstLine = cc_nounContentLower.split(".")[0]
            cityTypes = ["city","capital","town","vilage","settlement","parish","neighbourhood","neighborhood","borough","port","township","metropolis","quarter"]
            for cityType in cityTypes:
                if cityType in cc_nounContentFirstLine:
                    tagtype = "CIT"
                    url = wikipedia.page(str(cc_noun)).url
                    return tagtype, site
                else:
                    subdivisionTypes = ["state ","city-state","province","district","region","principality","municipality","sovereign","country"]
                    if tagtype == "":
                        for subdivisionType in subdivisionTypes:
                            if subdivisionType in cc_nounContentFirstLine:
                                tagtype = "COU"
                                url = wikipedia.page(str(cc_noun)).url
                                return tagtype, site
        except wikipedia.exceptions.DisambiguationError as e: #if word has more than one wikipedia page, met lesk of synsets
            list_text = []
            for lines in data:
                lines = lines.split()
                list_text.append(lines[3])
                joined_list = " ".join(list_text)
            sent = word_tokenize(joined_list)
            good_synset = lesk(sent, cc_noun, pos='n') #gives back the good synset of noun
            
            list_pos_ss = []
            for ss in wordnet.synsets(cc_noun, "n"):
                possible_ss = [ss, ss.definition()] #gives back all possible synsets with definition
                synset01 = [ss] #gives back all possible synsets
                
                for s in synset01:
                    list_pos_ss.append(ss) #makes list of all possible synsets (without defintion)
                
            for item in list_pos_ss:
                if item == good_synset: #checks whether good synset is in list of all possible synsets
                    good_def_list = item.definition().split() #gives back list of the definition of the good synset
                    
            cou_list = ["country","monarchy","republic","city-state","province","state","district","region","principality","municipality","sovereign","nation"]
            cou_checker = (any(elem in cou_list for elem in good_def_list))
            if cou_checker:
                tagytype = "COU"
                site = getWikipage(cc_noun)
                return tagtype, site
            else:
                tagtype = "CIT"
                site = getWikipage(cc_noun)
                return tagtype, site


def appendStrToEntFile(columns, path):
	"""
	Creates a new file and adds a string (row of space seperated values) 
	and a newline.

	columns	-- a list of strings
	path 	-- a path to a directory where a new file should be created or an existing file should be modified as sting
	"""
	newPath = path + "/en.tok.off.pos.ent2"
	newFile = open(newPath,"a")
	newFile.write(" ".join(columns))
	newFile.write("\n")
	newFile.close()


def main():
    path = os.getcwd() + '/development/'
    targetFile = 'en.tok.off.pos'
    os.chdir(path)
    for root, dirs, files in os.walk(path):
        for file in files:
            if file == targetFile:
                f = os.path.join(root, file)
                getEntity(root, f)

if __name__ == "__main__":
    main()
