import wikipedia
import os
import sys
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import wordnet
import spacy
import wikipedia
nlp = spacy.load("en")
import sys
import nltk
from nltk.tag.stanford import StanfordNERTagger
from nltk.wsd import lesk
from nltk import word_tokenize
import wikipedia #first pip install wikipedia
import pycountry #first pip install pycountry

def main():
    lijst = []
    noun = "New York"
    tagtype = "CIT"
    x = True #when x = false the function stops. So the output won't be 5 items long
    try:
        wiki = wikipedia.page(str(noun)).url
        print(wiki)
    except wikipedia.exceptions.PageError: #if there is no wikipedia page for the noun, gives back empty string
        print("")
    except wikipedia.exceptions.DisambiguationError as e: #more than one wikipedia page available
        if tagtype == "CIT" or tagtype == "COU":
            options = e.options
            first_five = options[:5] #prints out the first 5 options
            if tagtype == "COU":
                for option in first_five:
                    if "(state)" in option: #checks if wikipage should be the state or the city page like with Washington or New York
                        if x == True:
                            good = option
                            website = wikipedia.page(str(good)).url
                            x = False
                            print(website)
                    else:
                        if x == True:
                            website = wikipedia.page(str(e.options[0])).url
                            x = False
                            print(website)
                            
            else: #if not a country or state
                for option in first_five:
                    if x == True:
                        if "(state)" not in option:
                            good = option
                            website = wikipedia.page(str(good)).url
                            x = False
                            print(website) 
                
        else: #if tag is not COU or CIT
            if x == True:
                website = wikipedia.page(str(e.options[0])).url
                x = False
                print(website)
main()